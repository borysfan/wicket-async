package pl.edu.pw;

import java.io.IOException;

import javax.servlet.http.Cookie;

import org.apache.wicket.protocol.http.WebResponse;
import org.apache.wicket.util.string.AppendingStringBuffer;
import org.apache.wicket.util.time.Time;
import org.atmosphere.cpr.AtmosphereResponse;

public class AtmosphereWebResponse extends WebResponse {
	private AtmosphereResponse response;
	private final AppendingStringBuffer out;
	private boolean redirect;

	AtmosphereWebResponse(AtmosphereResponse response) {
		this.response = response;
		out = new AppendingStringBuffer(128);
	}

	@Override
	public void addCookie(Cookie cookie) {
		response.addCookie(cookie);
	}

	@Override
	public void clearCookie(Cookie cookie) {
		throw new UnsupportedOperationException();
	}

	@Override
	public CharSequence encodeURL(CharSequence url) {
		return response.encodeURL(url.toString());
	}

	@Override
	protected void sendRedirect(String url) throws IOException {
		out.clear();
		out.append("<ajax-response><redirect><![CDATA[" + url + "]]></redirect></ajax-response>");
		redirect = true;
	}

	@Override
	public void setContentLength(long length) {
		response.setContentLength((int) length);
	}

	@Override
	public void write(CharSequence string) {
		if (!redirect)
			out.append(string);
	}

	@Override
	public void write(AppendingStringBuffer asb) {
		if (!redirect)
			out.append(asb);
	}

	@Override
	public void setDateHeader(String header, long date) {
		response.setDateHeader(header, date);
	}

	@Override
	public void setHeader(String header, String value) {
		response.setHeader(header, value);
	}

	@Override
	public boolean isAjax() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setAjax(boolean ajax) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void disableCaching() {
		setDateHeader("Date", Time.now().getMilliseconds());
		setDateHeader("Expires", Time.START_OF_UNIX_TIME.getMilliseconds());
		setHeader("Pragma", "no-cache");
		setHeader("Cache-Control", "no-cache, no-store");
	}

	@Override
	public void redirect(String url) {
		throw new UnsupportedOperationException();
	}

}
