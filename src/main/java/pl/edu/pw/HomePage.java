package pl.edu.pw;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.wicket.Application;
import org.apache.wicket.IResourceListener;
import org.apache.wicket.MetaDataKey;
import org.apache.wicket.PageParameters;
import org.apache.wicket.RequestCycle;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.protocol.http.servlet.ServletWebRequest;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.cpr.AtmosphereResourceEventListener;
import org.atmosphere.cpr.HeaderConfig;
import org.atmosphere.cpr.Meteor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HomePage extends WebPage implements AtmosphereResourceEventListener, IResourceListener {
	private static final Logger log = LoggerFactory.getLogger(HomePage.class);
	private static final long serialVersionUID = 1L;
	private Label label;
	private String applicationKey;
	public static final MetaDataKey<String> ATMOSPHERE_UUID = new MetaDataKey<String>() {
		private static final long serialVersionUID = 1L;
	};

	/**
	 * Constructor that is invoked when page is invoked without a session.
	 * 
	 * @param parameters
	 *            Page parameters
	 */
	public HomePage(final PageParameters parameters) {
		applicationKey = WebApplication.get().getApplicationKey();
		label = new Label("message", "This clock updates the time using Atmosphere Meteor PUSH framework");
		label.setOutputMarkupId(true);
		add(label);

		Link b = new Link("link") {

			@Override
			public void onClick() {
				for (int i = 0; i < 100; i++) {
					((WicketPushApplication) WebApplication.get()).getAtmosphereEventBus().post(new Date());
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}

		};
		add(b);
		// add(new ClockPanel("clockPanel"));
	}

	public void update(AjaxRequestTarget target, Object event) {
		System.out.println("Hahaha");
		System.out.println(event);
		label.setDefaultModelObject(new Date());
		target.addComponent(label);
	}

	private AtmosphereEventBus findEventBus() {
		return ((WicketPushApplication) Application.get()).getAtmosphereEventBus();
	}

	public void onResourceRequested() {
		RequestCycle requestCycle = RequestCycle.get();
		ServletWebRequest request = (ServletWebRequest) requestCycle.getRequest();

		// Grab a Meteor
		Meteor meteor = Meteor.build(request.getHttpServletRequest());
		String uuid = meteor.getAtmosphereResource().uuid();
		this.getPage().setMetaData(ATMOSPHERE_UUID, uuid);
		findEventBus().registerPage(uuid, this.getPage());

		// Add us to the listener list.
		meteor.addListener(this);

		String transport = request.getHttpServletRequest().getHeader(HeaderConfig.X_ATMOSPHERE_TRANSPORT);
		if (HeaderConfig.LONG_POLLING_TRANSPORT.equalsIgnoreCase(transport)) {
			// request.getContainerRequest().setAttribute(ApplicationConfig.RESUME_ON_BROADCAST,
			// Boolean.TRUE);
			meteor.suspend(-1, false);
		} else {
			meteor.suspend(-1);
		}
	}

	public void onSuspend(AtmosphereResourceEvent event) {
		if (log.isInfoEnabled()) {
			String transport = event.getResource().getRequest().getHeader(HeaderConfig.X_ATMOSPHERE_TRANSPORT);
			HttpServletRequest req = event.getResource().getRequest();
			log.info(String.format("Suspending the %s response from ip %s:%s", transport == null ? "websocket" : transport, req.getRemoteAddr(),
					req.getRemotePort()));
		}
	}

	public void onResume(AtmosphereResourceEvent event) {
		if (log.isInfoEnabled()) {
			String transport = event.getResource().getRequest().getHeader("X-Atmosphere-Transport");
			HttpServletRequest req = event.getResource().getRequest();
			log.info(String.format("Resuming the %s response from ip %s:%s", transport == null ? "websocket" : transport, req.getRemoteAddr(),
					req.getRemotePort()));
		}
	}

	public void onDisconnect(AtmosphereResourceEvent event) {
		if (log.isInfoEnabled()) {
			String transport = event.getResource().getRequest().getHeader("X-Atmosphere-Transport");
			HttpServletRequest req = event.getResource().getRequest();
			log.info(String.format("%s connection dropped from ip %s:%s", transport == null ? "websocket" : transport, req.getRemoteAddr(), req.getRemotePort()));
		}
		// It is possible that the application has already been destroyed, in
		// which case
		// unregistration is no longer needed
		if (Application.get(applicationKey) != null) {
		}
	}

	public void onBroadcast(AtmosphereResourceEvent event) {
		log.info("onBroadcast: {}", event.getMessage());

		// If we are using long-polling, resume the connection as soon as we get
		// an event.
		String transport = event.getResource().getRequest().getHeader(HeaderConfig.X_ATMOSPHERE_TRANSPORT);

		if (HeaderConfig.LONG_POLLING_TRANSPORT.equalsIgnoreCase(transport)) {
			Meteor meteor = Meteor.lookup(event.getResource().getRequest());
			meteor.resume();
		}
	}

	public void onThrowable(AtmosphereResourceEvent event) {
		log.error(event.throwable().getMessage(), event.throwable());
	}

}
