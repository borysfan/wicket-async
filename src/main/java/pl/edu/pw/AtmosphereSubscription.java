package pl.edu.pw;

public class AtmosphereSubscription {
	private String componentPath;
	private String methodName;
	private Integer behaviorIndex;

	public AtmosphereSubscription(String componentPath, Integer behaviorIndex, String methodName) {
		this.componentPath = componentPath;
		this.methodName = methodName;
		this.behaviorIndex = behaviorIndex;
	}

	public String getComponentPath() {
		return componentPath;
	}

	public void setComponentPath(String componentPath) {
		this.componentPath = componentPath;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public Integer getBehaviorIndex() {
		return behaviorIndex;
	}

	public void setBehaviorIndex(Integer behaviorIndex) {
		this.behaviorIndex = behaviorIndex;
	}

}
