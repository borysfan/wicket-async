/*
 * Copyright 2012 Jeanfrancois Arcand
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package pl.edu.pw;

import java.util.Date;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.AbstractReadOnlyModel;
import org.apache.wicket.protocol.http.WebApplication;

/**
 * Simple panel.
 *
 * @author Andrey Belyaev
 */
public class ClockPanel extends Panel {

	private AjaxRequestTarget newAjaxRequestTarget;

	public ClockPanel(String id) {
        super(id);
		add(new BookmarkablePageLink<PushPage>("cometStart", PushPage.class));
        add(new Label("clock", new AbstractReadOnlyModel<String>() {
            @Override
            public String getObject() {
                return new Date().toString();
            }
        }));

    }

	public void subscribe() {
		newAjaxRequestTarget = WebApplication.get().newAjaxRequestTarget(getWebPage());
		/*
		 * Page page =
		 * (Page)Application.get().getMapperContext().getPageInstance
		 * (pageKey.getPageId()); AjaxRequestTarget target =
		 * WebApplication.get().newAjaxRequestTarget(page);
		 * executeHandlers(target, page);
		 */
	}
}
