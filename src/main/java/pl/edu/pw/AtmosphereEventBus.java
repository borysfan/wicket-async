package pl.edu.pw;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.wicket.Component;
import org.apache.wicket.Page;
import org.apache.wicket.RequestCycle;
import org.apache.wicket.Session;
import org.apache.wicket.application.IComponentOnBeforeRenderListener;
import org.apache.wicket.behavior.IBehavior;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.protocol.http.WicketFilter;
import org.apache.wicket.protocol.http.servlet.ServletWebRequest;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceFactory;
import org.atmosphere.cpr.Broadcaster;
import org.atmosphere.cpr.BroadcasterFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author borysfan
 * @Feb 2, 2013 @9:18:22 PM
 */
public class AtmosphereEventBus {
	private final static Logger log = LoggerFactory.getLogger(AtmosphereEventBus.class);

	private WebApplication webApplication;
	private Broadcaster broadcaster;
	private Map<PageKey, AtmosphereSubscription> subscriptions;
	private Map<String, PageKey> trackedPages;
	private List<ResourceRegistrationListener> registrationListeners = new CopyOnWriteArrayList<ResourceRegistrationListener>();

	public AtmosphereEventBus(WebApplication webApplication) {
		this.webApplication = webApplication;
		broadcaster = BroadcasterFactory.getDefault().lookupAll().iterator().next();
		subscriptions = new HashMap<PageKey, AtmosphereSubscription>();
		trackedPages = new HashMap<String, PageKey>();
		webApplication.addPreComponentOnBeforeRenderListener(new IComponentOnBeforeRenderListener() {

			public void onBeforeRender(Component component) {
				Method updateMethod = getUpdateMethod(component.getClass());
				if (updateMethod != null) {
					registerPage(component, updateMethod);
					Page page = component.getPage();
					if (!containsAtmoshphereBehavior(page)) {
						page.add(new AtmosphereComponentBehavior());
					}
				}
			}

		});
	}

	private boolean containsAtmoshphereBehavior(Page page) {
		boolean result = false;
		List<IBehavior> behaviors = page.getBehaviors();
		if (behaviors != null) {
			for (IBehavior behaviour : behaviors) {
				if (behaviour instanceof AtmosphereComponentBehavior) {
					result = true;
					break;
				}
			}
		}
		return result;
	}

	public synchronized void registerPage(String trackingId, Page page) {
		PageKey oldPage = trackedPages.remove(trackingId);
		PageKey pageKey = new PageKey(page.getId(), Session.get().getId());
		pageKey.setPage(page);
		if (oldPage != null && !oldPage.equals(pageKey)) {
			subscriptions.remove(oldPage);
			fireUnregistration(trackingId);
		}
		trackedPages.put(trackingId, pageKey);
		fireRegistration(trackingId, page);
		log.info("registered page {} for session {}", new Object[] { pageKey.getPageId(), pageKey.getSessionId() });
	}

	private synchronized void registerPage(Component component, Method updateMethod) {
		AtmosphereSubscription atmosphereSubscription = new AtmosphereSubscription(component.getPageRelativePath(), null, updateMethod.getName());
		PageKey pageKey = new PageKey(component.getPage().getId(), Session.get().getId());
		pageKey.setPage(component.getPage());
		subscriptions.put(pageKey, atmosphereSubscription);
	}

	private Method getUpdateMethod(Class<?> clazz) {
		Method[] methods = clazz.getMethods();
		if (methods != null) {
			for (Method method : methods) {
				if (method.getName().equals("update")) {
					return method;
				}
			}
		}
		return null;
	}

	public void post(Object event) {
		log.info("WebAppliaction exists: " + webApplication.exists());
		Collection<AtmosphereResource> atmosphereResources = broadcaster.getAtmosphereResources();
		if (atmosphereResources != null) {
			for (AtmosphereResource atmosphereResource : atmosphereResources) {
				post(event, atmosphereResource);
			}
		}
	}

	public void post(Object event, String resourceUuid) {
		AtmosphereResource resource = AtmosphereResourceFactory.getDefault().find(resourceUuid);
		if (resource != null) {
			post(event, resource);
		}
	}

	private void post(Object event, AtmosphereResource resource) {
		synchronized (this) {
			PageKey pageKey = trackedPages.get(resource.uuid());
			if (pageKey != null) {
				AtmosphereSubscription atmosphereSubscription = subscriptions.get(pageKey);
				post(resource, pageKey, atmosphereSubscription, event);
			} else {
				broadcaster.removeAtmosphereResource(resource);
			}
		}
	}

	private void post(AtmosphereResource resource, PageKey pageKey, AtmosphereSubscription atmosphereSubscription, Object event) {
		String filterPath = webApplication.getWicketFilter().getFilterConfig().getInitParameter(WicketFilter.FILTER_MAPPING_PARAM);
		filterPath = filterPath.substring(1, filterPath.length() - 1);
		HttpServletRequest httpServletRequest = new HttpServletRequestWrapper(resource.getRequest()) {

			@Override
			public String getContextPath() {
				String ret = super.getContextPath();
				return ret == null ? "" : ret;
			}

		};
		ServletWebRequest servletWebRequest = new ServletWebRequest(httpServletRequest);
		AtmosphereWebRequest request = new AtmosphereWebRequest(servletWebRequest, pageKey, atmosphereSubscription, event);
		AtmosphereWebResponse response = new AtmosphereWebResponse(resource.getResponse());
		RequestCycle newRequestCycle = webApplication.newRequestCycle(request, response);
		try {
			// IRequestCycleProcessor processor =
			// newRequestCycle.getProcessor();
			// processor.processEvents(newRequestCycle);
			// newRequestCycle.(new AtmosphereRequestTarget(pageKey,
			// event, atmosphereSubscription));
		} finally {
			newRequestCycle.detach();
			broadcaster.broadcast(response.toString(), resource);
		}

	}

	public void addRegistrationListener(ResourceRegistrationListener listener) {
		registrationListeners.add(listener);
	}

	public void removeRegistrationListener(ResourceRegistrationListener listener) {
		registrationListeners.add(listener);
	}

	private void fireRegistration(String uuid, Page page) {
		for (ResourceRegistrationListener curListener : registrationListeners) {
			curListener.resourceRegistered(uuid, page);
		}
	}

	private void fireUnregistration(String uuid) {
		for (ResourceRegistrationListener curListener : registrationListeners) {
			curListener.resourceUnregistered(uuid);
		}
	}

}
