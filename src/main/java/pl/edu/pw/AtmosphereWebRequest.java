package pl.edu.pw;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.wicket.protocol.http.WebRequest;
import org.apache.wicket.util.lang.Bytes;

public class AtmosphereWebRequest extends WebRequest {

	private WebRequest wrappedRequest;
	private PageKey pageKey;
	private AtmosphereSubscription atmosphereSubscription;
	private Object event;

	public AtmosphereWebRequest(WebRequest wrappedRequest, PageKey pageKey, AtmosphereSubscription atmosphereSubscription, Object event) {
		this.wrappedRequest = wrappedRequest;
		this.pageKey = pageKey;
		this.atmosphereSubscription = atmosphereSubscription;
		this.event = event;
	}

	public WebRequest getWrappedRequest() {
		return wrappedRequest;
	}

	public PageKey getPageKey() {
		return pageKey;
	}

	public AtmosphereSubscription getAtmosphereSubscription() {
		return atmosphereSubscription;
	}

	public Object getEvent() {
		return event;
	}

	@Override
	public HttpServletRequest getHttpServletRequest() {
		return wrappedRequest.getHttpServletRequest();
	}

	@Override
	public Locale getLocale() {
		return wrappedRequest.getLocale();
	}

	@Override
	public String getParameter(String key) {
		return wrappedRequest.getParameter(key);
	}

	@Override
	public Map<String, String[]> getParameterMap() {
		return wrappedRequest.getParameterMap();
	}

	@Override
	public String[] getParameters(String key) {
		return wrappedRequest.getParameters(key);
	}

	@Override
	public String getServletPath() {
		return wrappedRequest.getServletPath();
	}

	@Override
	public WebRequest newMultipartWebRequest(Bytes maxSize) {
		return wrappedRequest.newMultipartWebRequest(maxSize);
	}

	@Override
	public boolean isAjax() {
		return true;
	}

	@Override
	public String getPath() {
		return wrappedRequest.getPath();
	}

	@Override
	public String getRelativePathPrefixToContextRoot() {
		return wrappedRequest.getRelativePathPrefixToContextRoot();
	}

	@Override
	public String getRelativePathPrefixToWicketHandler() {
		return wrappedRequest.getRelativePathPrefixToWicketHandler();
	}

	@Override
	public String getURL() {
		return wrappedRequest.getURL();
	}

	@Override
	public String getQueryString() {
		return wrappedRequest.getQueryString();
	}

}
