package pl.edu.pw;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.wicket.Component;
import org.apache.wicket.IRequestTarget;
import org.apache.wicket.Page;
import org.apache.wicket.RequestCycle;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.protocol.http.WebApplication;

public class AtmosphereRequestTarget implements IRequestTarget {

	private PageKey pageKey;
	private Object event;
	private AtmosphereSubscription subscription;

	public AtmosphereRequestTarget(PageKey pageKey, Object event, AtmosphereSubscription subscription) {
		this.pageKey = pageKey;
		this.event = event;
		this.subscription = subscription;
	}

	public void respond(RequestCycle requestCycle) {
		if (requestCycle.getRequest() instanceof AtmosphereWebRequest) {
			AtmosphereWebRequest atmosphereWebRequest = (AtmosphereWebRequest) requestCycle.getRequest();
			Page page = atmosphereWebRequest.getPageKey().getPage();
			AjaxRequestTarget ajaxRequestTarget = WebApplication.get().newAjaxRequestTarget(page);
			executeHandlers(ajaxRequestTarget, page);
		}
	}

	private void executeHandlers(AjaxRequestTarget ajaxRequestTarget, Page page) {
		Component component = page.get(subscription.getComponentPath());
		invokeMethod(ajaxRequestTarget, subscription, component);

	}

	private void invokeMethod(AjaxRequestTarget ajaxRequestTarget, AtmosphereSubscription subscription2, Component component) {
		Method[] methods = component.getClass().getMethods();
		if (methods != null) {
			for (Method method : methods) {
				if (method.getName().equals(subscription.getMethodName())) {
					try {
						method.invoke(component, ajaxRequestTarget, event);
					} catch (IllegalArgumentException e) {
						throw new WicketRuntimeException(e);
					} catch (IllegalAccessException e) {
						throw new WicketRuntimeException(e);
					} catch (InvocationTargetException e) {
						throw new WicketRuntimeException(e);
					}
				}
			}
		}

	}

	public void detach(RequestCycle requestCycle) {

	}

}
