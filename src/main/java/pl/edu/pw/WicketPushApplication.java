/*
 * Copyright 2012 Jeanfrancois Arcand
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package pl.edu.pw;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.wicket.Request;
import org.apache.wicket.RequestContext;
import org.apache.wicket.RequestCycle;
import org.apache.wicket.Response;
import org.apache.wicket.Session;
import org.apache.wicket.protocol.http.WebApplication;

public class WicketPushApplication extends WebApplication {

	private AtmosphereEventBus atmosphereEventBus;

	public Class<HomePage> getHomePage() {
		return HomePage.class;
	}

	@Override
	protected void init() {
		super.init();
		atmosphereEventBus = new AtmosphereEventBus(this);
		ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(60);
		final Runnable beeper = new Runner(this);
		scheduler.scheduleWithFixedDelay(beeper, 10, 10, TimeUnit.SECONDS);
	}

	public AtmosphereEventBus getAtmosphereEventBus() {
		return atmosphereEventBus;
	}

	public void setAtmosphereEventBus(AtmosphereEventBus atmosphereEventBus) {
		this.atmosphereEventBus = atmosphereEventBus;

	}

	public static class Runner implements Runnable {

		private WicketPushApplication application;

		Runner(WicketPushApplication wicketPushApplication) {
			application = wicketPushApplication;
		}

		public void run() {
			try {
				if (application == null) {
					application = (WicketPushApplication) Session.get().getApplication();
				}
				application.getAtmosphereEventBus().post(new Date());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public final RequestCycle createRequestCycle(final Request request, final Response response) {
		RequestContext requestContext = RequestContext.get();
		RequestCycle requestCycle = newRequestCycle(request, response);
		// requestCycle.getgetListeners().add(requestCycleListeners);
		// requestCycle.getListeners().add(new AbstractRequestCycleListener() {
		// @Override
		// public void onDetach(final RequestCycle requestCycle) {
		// if (Session.exists()) {
		// Session.get().getPageManager().commitRequest();
		// }
		// }
		//
		// @Override
		// public void onEndRequest(RequestCycle cycle) {
		// if (Application.exists()) {
		// IRequestLogger requestLogger = Application.get().getRequestLogger();
		// if (requestLogger != null) {
		// requestLogger.requestTime((System.currentTimeMillis() -
		// cycle.getStartTime()));
		// }
		// }
		// }
		// });
		return requestCycle;
	}

}
