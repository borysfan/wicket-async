package pl.edu.pw;

import javax.servlet.http.HttpServletRequest;

import org.apache.wicket.Application;
import org.apache.wicket.Component;
import org.apache.wicket.IResourceListener;
import org.apache.wicket.MetaDataKey;
import org.apache.wicket.Page;
import org.apache.wicket.RequestCycle;
import org.apache.wicket.ResourceReference;
import org.apache.wicket.behavior.AbstractBehavior;
import org.apache.wicket.markup.html.IHeaderResponse;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.protocol.http.servlet.ServletWebRequest;
import org.atmosphere.cpr.AtmosphereResource;
import org.atmosphere.cpr.AtmosphereResourceEvent;
import org.atmosphere.cpr.AtmosphereResourceEventListener;
import org.atmosphere.cpr.HeaderConfig;
import org.atmosphere.cpr.Meteor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AtmosphereComponentBehavior extends AbstractBehavior implements IResourceListener, AtmosphereResourceEventListener {
	private Logger log = LoggerFactory.getLogger(AtmosphereComponentBehavior.class);
	private static final long serialVersionUID = -2241131274283397585L;

	private Component component;
	private String applicationKey;

	public AtmosphereComponentBehavior() {
		applicationKey = WebApplication.get().getApplicationKey();
	}

	public static final MetaDataKey<String> ATMOSPHERE_UUID = new MetaDataKey<String>() {
		private static final long serialVersionUID = 1L;
	};

	@Override
	public void renderHead(IHeaderResponse response) {
		response.renderJavascriptReference(JQueryJavascriptReference.getInstance());
		response.renderJavascriptReference(WicketEventJavascriptReference.getInstance());
		response.renderJavascriptReference(JQueryAtmosphereJavascriptReference.getInstance());
		response.renderJavascriptReference(WicketAtmosphereJavascriptReference.getInstance());
		String options = "{\"url\":\"" + component.urlFor(this, IResourceListener.INTERFACE).toString() + "\"}";
		String javascript = "$('#" + component.getMarkupId() + "').wicketAtmosphere(" + options + ")";
		String js = "Wicket.Event.add(window, \"domready\", function(event) { " + javascript + ";});";
		response.renderJavascript(js, "atmosphereJS");
	}

	public static class WicketAtmosphereJavascriptReference extends ResourceReference {

		private static final long serialVersionUID = -3582288170565231416L;

		private static final String RESOURCE_NAME = "jquery.wicketatmosphere.js";
		private static final WicketAtmosphereJavascriptReference instance = new WicketAtmosphereJavascriptReference();

		public static WicketAtmosphereJavascriptReference getInstance() {
			return instance;
		}

		private WicketAtmosphereJavascriptReference() {
			super(WicketAtmosphereJavascriptReference.class, RESOURCE_NAME);
		}
	}

	public static class JQueryAtmosphereJavascriptReference extends ResourceReference {

		private static final long serialVersionUID = -3956514712038732060L;

		private static final String RESOURCE_NAME = "jquery.atmosphere.js";
		private static final JQueryAtmosphereJavascriptReference instance = new JQueryAtmosphereJavascriptReference();

		public static JQueryAtmosphereJavascriptReference getInstance() {
			return instance;
		}

		private JQueryAtmosphereJavascriptReference() {
			super(JQueryAtmosphereJavascriptReference.class, RESOURCE_NAME);
		}

	}

	public static class JQueryJavascriptReference extends ResourceReference {

		private static final long serialVersionUID = -6469430426783095463L;
		private static final String RESOURCE_NAME = "jquery-1.7.2.js";
		private static final JQueryJavascriptReference instance = new JQueryJavascriptReference();

		public static JQueryJavascriptReference getInstance() {
			return instance;
		}

		private JQueryJavascriptReference() {
			super(JQueryJavascriptReference.class, RESOURCE_NAME);
		}

	}
	
	public static class WicketEventJavascriptReference extends ResourceReference {

		private static final long serialVersionUID = -4790823713566540384L;
		private static final String RESOURCE_NAME = "wicket.event.js";
		private static final WicketEventJavascriptReference instance = new WicketEventJavascriptReference();

		public static WicketEventJavascriptReference getInstance() {
			return instance;
		}

		private WicketEventJavascriptReference() {
			super(WicketEventJavascriptReference.class, RESOURCE_NAME);
		}

	}

	@Override
	public boolean getStatelessHint(Component component) {
		return false;
	}

	@Override
	public void bind(Component component) {
		this.component = component;
	}

	private AtmosphereEventBus findEventBus() {
		return ((WicketPushApplication) Application.get()).getAtmosphereEventBus();
	}

	public void onResourceRequested() {
		RequestCycle requestCycle = RequestCycle.get();
		ServletWebRequest request = (ServletWebRequest) requestCycle.getRequest();

		// Grab a Meteor
		Meteor meteor = Meteor.build(request.getHttpServletRequest());
		String uuid = meteor.getAtmosphereResource().uuid();
		component.getPage().setMetaData(ATMOSPHERE_UUID, uuid);
		findEventBus().registerPage(uuid, component.getPage());

		// Add us to the listener list.
		meteor.addListener(this);

		String transport = request.getHttpServletRequest().getHeader(HeaderConfig.X_ATMOSPHERE_TRANSPORT);
		if (HeaderConfig.LONG_POLLING_TRANSPORT.equalsIgnoreCase(transport)) {
			// request.getContainerRequest().setAttribute(ApplicationConfig.RESUME_ON_BROADCAST,
			// Boolean.TRUE);
			meteor.suspend(-1, false);
		} else {
			meteor.suspend(-1);
		}
	}

	public void onSuspend(AtmosphereResourceEvent event) {
		if (log.isInfoEnabled()) {
			String transport = event.getResource().getRequest().getHeader(HeaderConfig.X_ATMOSPHERE_TRANSPORT);
			HttpServletRequest req = event.getResource().getRequest();
			log.info(String.format("Suspending the %s response from ip %s:%s", transport == null ? "websocket" : transport, req.getRemoteAddr(),
					req.getRemotePort()));
		}
	}

	public void onResume(AtmosphereResourceEvent event) {
		if (log.isInfoEnabled()) {
			String transport = event.getResource().getRequest().getHeader("X-Atmosphere-Transport");
			HttpServletRequest req = event.getResource().getRequest();
			log.info(String.format("Resuming the %s response from ip %s:%s", transport == null ? "websocket" : transport, req.getRemoteAddr(),
					req.getRemotePort()));
		}
	}

	public void onDisconnect(AtmosphereResourceEvent event) {
		if (log.isInfoEnabled()) {
			String transport = event.getResource().getRequest().getHeader("X-Atmosphere-Transport");
			HttpServletRequest req = event.getResource().getRequest();
			log.info(String.format("%s connection dropped from ip %s:%s", transport == null ? "websocket" : transport, req.getRemoteAddr(), req.getRemotePort()));
		}
		// It is possible that the application has already been destroyed, in
		// which case
		// unregistration is no longer needed
		if (Application.get(applicationKey) != null) {
		}
	}

	public void onBroadcast(AtmosphereResourceEvent event) {
		log.info("onBroadcast: {}", event.getMessage());

		// If we are using long-polling, resume the connection as soon as we get
		// an event.
		String transport = event.getResource().getRequest().getHeader(HeaderConfig.X_ATMOSPHERE_TRANSPORT);

		if (HeaderConfig.LONG_POLLING_TRANSPORT.equalsIgnoreCase(transport)) {
			Meteor meteor = Meteor.lookup(event.getResource().getRequest());
			meteor.resume();
		}
	}

	public void onThrowable(AtmosphereResourceEvent event) {
		log.error(event.throwable().getMessage(), event.throwable());
	}

	/**
	 * Find the Atmosphere UUID for the suspended connection for the given page
	 * (if any).
	 * 
	 * @param page
	 * @return The UUID of the Atmosphere Resource, or null if no resource is
	 *         suspended for the page.
	 */
	public static String getUUID(Page page) {
		return page.getMetaData(ATMOSPHERE_UUID);
	}

	/**
	 * @param resource
	 * @return the unique id for the given suspended connection
	 * @deprecated use {@link AtmosphereResource#uuid()}
	 */
	@Deprecated
	public static String getUUID(AtmosphereResource resource) {
		return resource.uuid();
	}

}
