package pl.edu.pw;

import org.apache.wicket.ResourceReference;
import org.apache.wicket.behavior.AbstractBehavior;
import org.apache.wicket.markup.html.IHeaderResponse;

/**
 * @author borysfan
 * @Feb 2, 2013 @5:49:02 PM
 * 
 *      Class is adding wicket.atmosphere.js files to web page using Wikcet
 *      Behaviours. Behavior first is trying to add jquery.atmosphere.js library
 *      for atmosphere and next jquery.wicketatmosphere.js.
 */
public class AtmosphereBehavior extends AbstractBehavior {

	private static final long serialVersionUID = 5258357533407329048L;

	@Override
	public void renderHead(IHeaderResponse response) {
		response.renderJavascriptReference(JQueryJavascriptReference.getInstance());
		response.renderJavascriptReference(JQueryAtmosphereJavascriptReference.getInstance());
		response.renderJavascriptReference(WicketAtmosphereJavascriptReference.getInstance());
	}

	public static class WicketAtmosphereJavascriptReference extends ResourceReference {

		private static final long serialVersionUID = -3582288170565231416L;

		private static final String RESOURCE_NAME = "jquery.wicketatmosphere.js";
		private static final WicketAtmosphereJavascriptReference instance = new WicketAtmosphereJavascriptReference();

		public static WicketAtmosphereJavascriptReference getInstance() {
			return instance;
		}

		private WicketAtmosphereJavascriptReference() {
			super(WicketAtmosphereJavascriptReference.class, RESOURCE_NAME);
		}
	}

	public static class JQueryAtmosphereJavascriptReference extends ResourceReference {

		private static final long serialVersionUID = -3956514712038732060L;

		private static final String RESOURCE_NAME = "jquery.atmosphere.js";
		private static final JQueryAtmosphereJavascriptReference instance = new JQueryAtmosphereJavascriptReference();

		public static JQueryAtmosphereJavascriptReference getInstance() {
			return instance;
		}

		private JQueryAtmosphereJavascriptReference() {
			super(JQueryAtmosphereJavascriptReference.class, RESOURCE_NAME);
		}

	}
	
	public static class JQueryJavascriptReference extends ResourceReference {

		private static final long serialVersionUID = -6469430426783095463L;
		private static final String RESOURCE_NAME = "jquery-1.7.2.js";
		private static final JQueryJavascriptReference instance = new JQueryJavascriptReference();

		public static JQueryJavascriptReference getInstance() {
			return instance;
		}

		private JQueryJavascriptReference() {
			super(JQueryJavascriptReference.class, RESOURCE_NAME);
		}

	}
}
